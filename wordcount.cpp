/*
 * CSc103 Project 3: wordcount++
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: cplusplus.com, C++ Advanced COurse:43-Sets(Youtube video)
 * stackoverflow.com/questions/18410234/how-does-one-represent-the
 * cplusplus.com/reference/string/string/substr
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 8
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;

// write this function to help you out with the computation.

unsigned long countWords(const string& s)
{
	unsigned long wordsCounted = 0; // Counts the first word.
	for(size_t i = 0; i < s.size(); i++)
	{
		unsigned long a = s[i]; // assign a to s at index i.
		unsigned long b = s[i+1]; // assign b to s at index i plus 1.
		if (a != ' ' || a != '\0' || a != '\t' || a!= '\n') // if s at index i is not blank space, end line, tab or new line. 
		{
			if (b == ' ' || b == '\0' || b == '\t' || b == '\n') // if s at index i plus 1 is a blank space, end line , tab or new line.
			{
				if (a != ' ') // if s at index i does is not a blank space.
					{
						wordsCounted++; // increment wordsCounted by 1.
					}
			}
		}
	}
	return wordsCounted; // returns the values of wordsCOunts.
}

unsigned long countCharacters(const string& s)
{
	unsigned long charactersCounted = 0; // Counts the first character.

	for(size_t i = 0; i < s.size(); i++)
	{
		if(s[i] != ' ' || s[i] == ' ') // everytime a characters is encountered, other than a whitespace, do the inside of the if statement.
		{
			charactersCounted++; // increment charactersCounted by 1.
		}
	}
	return charactersCounted; // returns the value of charactersCounted.
}

unsigned long countUniqueLines(const string& s, set<string>& wl)
{
	wl.insert(s); // insert the lines of words we type into wl at index s.
	return wl.size(); // return the value of the size of the wl.
}

unsigned long countUniqueWords(const string& s, set<string>& wl)
{
	string tempstring;     //this string will hold each word taken from the string
	unsigned long b = 0;   //positioning for the beginning of each word
	for(size_t i = 0; i < s.size(); i++)
	{
		if(s[i] == ' ' || s[i] == '\t')   //checking for spaces and tabs inbetween each word
		{
			tempstring = s.substr(b,(i-b));        //assigning the string the word
			for(size_t z = 0; z < tempstring.size(); z++)    //checking if the string is blank
			{
				if(tempstring[z] != 0)    //string is not blank, insert the word
				{
					wl.insert(tempstring);
				}
			}
			b = (i+1);        // reposition the beginning of the next word
		}
	}
	tempstring = s.substr(b);   //catching the last word of each line, as there is space at the end
	for(size_t x = 0; x < tempstring.size(); x++)  //checking if the string is blank
	{
		if(tempstring[x] != 0)    //word is not blank, insert word
		{
			wl.insert(tempstring);
		}
	}
	return wl.size();    //gives the number of unique words
}

int main()
{
	unsigned long lines = 0;	// initialized lines to 0.
	unsigned long characters = 0;  // initialized characters to 0.
	unsigned long words = 0;	// initialized words to 0.
	unsigned long uniLines = 0; // initialized uniLines to 0.
	unsigned long uniWords = 0; // initialized uniWords to 0.

	string line;	// initialized a string variable called line.
	set<string> wordList;	// initialized a set that passes a string datatype called wordList.
	set<string> uniWordList; // set that countains unique words labled uwordlist

	while(getline(cin,line))
	{
		lines++;	// increment the number of lines each time a new line of words are typed.
		words += countWords(line);	// counts the total number of words that are entered.
		characters++;	// increments character every time a new line of words is entered.
		characters += line.size();	// counts the total number of characters that are entered.
		uniLines = countUniqueLines(line, wordList);	// count the total number of unique lines in a series of multple lines being entered.
		uniWords = countUniqueWords(line, uniWordList); // count the total number of unique words from a series of lines of word entered.
	}
	cout << lines << "\t" << words << "\t" << characters << "\t" << uniLines << "\t" << uniWords << endl;
	return 0;
}